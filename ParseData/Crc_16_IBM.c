#include "Crc_16_IBM.h"

static uint16_t crc16_update(uint16_t crc, uint8_t a)
{
	crc ^= a;
	for (int i = 0; i < 8; ++i)
	{
		if (crc & 1)
			crc = (crc >> 1) ^ 0xA001;
		else
			crc = (crc >> 1);
	}
	return crc;
}

uint16_t crc(const uint8_t* buf, int len)
{
	uint16_t crc = 0xffff;
	while (len--)
	{
		crc = crc16_update(crc, *buf++);
	}
	return crc;
}
