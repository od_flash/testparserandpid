#include "ParseData.h"
#include "Crc_16_IBM.h"

#include <memory.h>

static const int IS_BIG_ENDIAN()
{
	return (*(uint8_t*)&(uint16_t) { 1 } == 0) ? 1 : 0;
}

static const char UUID = 'P'; // b01010000
static const uint16_t LENGHT = 17;
static const uint16_t MAX_VALUE_OUTPUT_CURENT = 750;
static const uint16_t MAX_VALUE_BIT_SK3 = 3;

static void parseUInt32LE(uint32_t* val, const uint8_t** buf)
{
	*val = *(uint32_t*)(*buf);
	*buf += sizeof(*val);
}

static void parseUInt32BE(uint32_t* val, const uint8_t** buf)
{
	const uint8_t* b = *buf;

	*val = 0;
	int cnt = sizeof(*val);
	while (cnt--)
	{
		*val <<= 8;
		*val |= *b++ << cnt;
	}

	*buf += sizeof(*val);
}

static void parseUInt16LE(uint16_t* val, const uint8_t** buf)
{
	*val = *(uint16_t*)(*buf);
	*buf += sizeof(*val);
}

static void parseUInt16BE(uint16_t* val, const uint8_t** buf)
{
	const uint8_t* b = *buf;

	*val = 0;
	int cnt = sizeof(*val);
	while (cnt--)
	{
		*val <<= 8;
		*val |= *b++ << cnt;
	}

	*buf += sizeof(*val);
}

static void (*parseUInt32)(uint32_t*, const uint8_t**);
static void (*parseUInt16)(uint16_t*, const uint8_t**);

static const int init()
{
	parseUInt32 = (IS_BIG_ENDIAN() == 1) ? parseUInt32BE : parseUInt32LE;
	parseUInt16 = (IS_BIG_ENDIAN() == 1) ? parseUInt16BE : parseUInt16LE;
	return 1;
}

int8_t fnParseData(const uint8_t* buf, TData* data)
{
	static const TData emptyData = { 0 };
	static volatile int isInited = 0;

	if (isInited == 0)
	{
		isInited = init();
	}

	const uint8_t* bufStart = buf;
	do
	{
		{
			data->uuid = (char)*buf++;
			if (data->uuid != UUID) break;
		}

		{
			data->size = *buf++;
			if (data->size != LENGHT) break;
		}

		{
			if (crc(bufStart, LENGHT) != 0) break;
		}

		{
			uint32_t timeStampUnix;
			parseUInt32(&timeStampUnix, &buf);
			data->timeStampUnix = (time_t)timeStampUnix;
		}

		parseUInt32(&data->energyMeter, &buf);

		{
			data->sk3.value = *buf++;
			if (data->sk3.value > MAX_VALUE_BIT_SK3) break;
		}

		{
			parseUInt16(&data->outputCurentInMvDiv10, &buf);
			if (data->outputCurentInMvDiv10 > MAX_VALUE_OUTPUT_CURENT)
				break;
		}

		parseUInt16(&data->countCurrentEvent, &buf);

		parseUInt16(&data->crc, &buf);

		return 1;

	} while (0);

	*data = emptyData;
	return -1;
}
