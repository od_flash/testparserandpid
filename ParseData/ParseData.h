#pragma once

#include <stdint.h>
#include <time.h>

typedef struct DigitalSK3
{
	uint8_t door : 1;
	uint8_t voltage : 1;
} TDigitalSK3;

typedef union SK3
{
	TDigitalSK3 dSK3;
	uint8_t value;
} TSK3;

typedef struct Data
{
	char uuid;
	uint8_t size;
	time_t timeStampUnix;
	uint32_t energyMeter;
	TSK3 sk3;
	uint16_t outputCurentInMvDiv10;
	uint16_t countCurrentEvent;
	uint16_t crc;
} TData;

extern int8_t fnParseData(const uint8_t*, TData*);