#include "pch.h"
extern "C"
{
#include "..\ParseData\ParseData.h"
#include "..\ParseData\Crc_16_IBM.h"
}

bool operator == (const TData& s1, const TData& s2)
{
	bool res = (s1.uuid == s2.uuid);
	res = res && (s1.size == s2.size);
	res = res && (s1.timeStampUnix == s2.timeStampUnix);
	res = res && (s1.energyMeter == s2.energyMeter);
	res = res && (s1.sk3.value == s2.sk3.value);
	res = res && (s1.outputCurentInMvDiv10 == s2.outputCurentInMvDiv10);
	res = res && (s1.countCurrentEvent == s2.countCurrentEvent);
	// res = res && (s1.crc == s2.crc);
	return res;
}

static bool dataToBuf(uint8_t* s, TData& data)
{
	uint8_t* buf = s;

	*buf++ = data.uuid;
	*buf++ = data.size;
	*(uint32_t*)buf = (uint32_t)data.timeStampUnix; buf += sizeof(uint32_t);
	*(uint32_t*)buf = data.energyMeter; buf += sizeof(uint32_t);
	*buf++ = data.sk3.value;
	*(uint16_t*)buf = data.outputCurentInMvDiv10; buf += sizeof(uint16_t);
	*(uint16_t*)buf = data.countCurrentEvent; buf += sizeof(uint16_t);

	data.crc = crc(s, int(buf - s));
	*(uint16_t*)buf = data.crc; //crc

	return true;
}

TEST(ParseData, Ok) {

	TData data = {};
	{
		data.uuid = 'P';
		data.size = 17;
		data.timeStampUnix = 1111;
		data.energyMeter = 700;
		data.sk3.value = 3;
		data.outputCurentInMvDiv10 = 340U;
		data.countCurrentEvent = 1024U;
	}

	uint8_t buf[17] = {};
	EXPECT_TRUE(dataToBuf(buf, data));

	{
		TData dataOut;
		EXPECT_TRUE(fnParseData(buf, &dataOut) != -1);
		EXPECT_TRUE(dataOut == data);
	}
}

TEST(ParseData, CrcError) {

	TData data = {};
	{
		data.uuid = 'P';
		data.size = 17;
		data.timeStampUnix = 1111;
		data.energyMeter = 700;
		data.sk3.value = 3;
		data.outputCurentInMvDiv10 = 340U;
		data.countCurrentEvent = 1024U;
	}

	uint8_t buf[17] = {};
	EXPECT_TRUE(dataToBuf(buf, data));

	*(uint16_t*)&buf[15] = data.crc - 3; //crc

	{
		TData dataOut;
		EXPECT_TRUE(fnParseData(buf, &dataOut) == -1);
	}
}

TEST(ParseData, UuidError) {

	TData data = {};
	{
		data.uuid = 'P';
		data.size = 17;
		data.timeStampUnix = 1111;
		data.energyMeter = 700;
		data.sk3.value = 3;
		data.outputCurentInMvDiv10 = 340U;
		data.countCurrentEvent = 1024U;
	}

	data.uuid = 'D';
	uint8_t buf[17] = {};
	EXPECT_TRUE(dataToBuf(buf, data));

	{
		TData dataOut;
		EXPECT_TRUE(fnParseData(buf, &dataOut) == -1);
	}
}

TEST(ParseData, SizeError) {

	TData data = {};
	{
		data.uuid = 'P';
		data.size = 17;
		data.timeStampUnix = 1111;
		data.energyMeter = 700;
		data.sk3.value = 3;
		data.outputCurentInMvDiv10 = 340U;
		data.countCurrentEvent = 1024U;
	}

	data.size = 13;
	uint8_t buf[17] = {};
	EXPECT_TRUE(dataToBuf(buf, data));

	{
		TData dataOut;
		EXPECT_TRUE(fnParseData(buf, &dataOut) == -1);
	}
}

TEST(ParseData, OutputCurentRangeError) {

	TData data = {};
	{
		data.uuid = 'P';
		data.size = 17;
		data.timeStampUnix = 1111;
		data.energyMeter = 7000;
		data.sk3.value = 3;
		data.outputCurentInMvDiv10 = 340U;
		data.countCurrentEvent = 1024U;
	}

	data.outputCurentInMvDiv10 = 800;
	uint8_t buf[17] = {};
	EXPECT_TRUE(dataToBuf(buf, data));

	{
		TData dataOut;
		EXPECT_TRUE(fnParseData(buf, &dataOut) == -1);
	}
}

TEST(ParseData, SK3RangeError) {

	TData data = {};
	{
		data.uuid = 'P';
		data.size = 17;
		data.timeStampUnix = 1111;
		data.energyMeter = 700;
		data.sk3.value = 3;
		data.outputCurentInMvDiv10 = 340U;
		data.countCurrentEvent = 1024U;
	}

	data.sk3.value = 10;
	uint8_t buf[17] = {};
	EXPECT_TRUE(dataToBuf(buf, data));

	{
		TData dataOut;
		EXPECT_TRUE(fnParseData(buf, &dataOut) == -1);
	}
}
