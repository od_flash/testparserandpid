#include "pch.h"
extern "C"
{
#include "..\PID\PID.h"
}

#include <time.h>

class HeaterEmulator
{
public:

	HeaterEmulator(double ambientTemp = 20, double power = 0.5, double kTemp = 0.1 / 60.0, double kPowerOn = 10.0 / 60.0)
		:m_ambientTemp(ambientTemp)
		, m_currentTemp(ambientTemp)
		, m_maxPower(power)
		, m_kTemp(kTemp)
		, m_currentPower(0.0)
		, m_kPowerOn(kPowerOn)
		, m_kPowerOff(kPowerOn * 5.0)
		, m_prevTime(0)
		, m_isPowerOn(false)
	{
	}

	void powerOn()
	{
		m_isPowerOn = true;
	}

	void powerOff()
	{
		m_isPowerOn = false;
	}

	void update(time_t time)
	{
		auto deltaTime = (time - m_prevTime);
		{
			m_currentPower += (m_isPowerOn ? m_kPowerOn : -m_kPowerOff) * deltaTime;
			if (m_currentPower > m_maxPower)
			{
				m_currentPower = m_maxPower;
			}
			else if (m_currentPower < 0)
			{
				m_currentPower = 0;
			}
		}

		m_currentTemp = m_currentTemp + m_currentPower * deltaTime + m_kTemp * (m_ambientTemp - m_currentTemp) * deltaTime;

		m_prevTime = time;
	}

	int8_t getTemp() 
	{ 
		if (m_currentTemp < -127)
		{
			return -127;
		}

		if (m_currentTemp > 127)
		{
			return 127;
		}

		return int8_t(m_currentTemp + 0.5); 
	}

private:
	bool m_isPowerOn;
	time_t m_prevTime;

	double m_kPowerOn;
	double m_kPowerOff;

	double m_currentPower;
	double m_maxPower;

	double m_kTemp;
	double m_currentTemp;
	double m_ambientTemp;
};

TEST(PID, p1) {
	time_t time = 0;
	time_t stepT = 1;

	setK(stepT, 5.0 / 1.0, 1.0 / 1.0, 1.0 / 2.0);

	HeaterEmulator he;

	while (time < 60 * 60 * 2)
	{
		he.update(time);
		auto T = he.getTemp();
		bool u = control(60, T, time);
		if (u)
		{
			he.powerOn();
		}
		else
		{
			he.powerOff();
		}
		time += stepT;

		std::cout << "T: " << T << " U: " << u << std::endl;
	};
}
