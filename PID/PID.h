#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <time.h>

extern void setK(time_t ts, double kp, double ki, double kd);

extern bool control(int8_t T, int8_t currentT, time_t time);

