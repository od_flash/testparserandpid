#include "PID.h"

typedef struct PointPID
{
	time_t time;
	double err;
	double iT;
	bool u;
} TPointPID;

static bool isStarted = false;

static double kP = 0.0;
static double kI = 0.0;
static double kD = 0.0;

static double U = 10.0;
static double iTMax = 5.0;
static double iTMin = -5.0;

static time_t timeStep = 0;

static TPointPID prev = { 0 };

void setK(time_t ts, double kp, double ki, double kd)
{
	isStarted = false;

	kP = kp;
	kI = ki;
	kD = kd;
	timeStep = ts;

	return;
}

// TODO: This is an example of a library function
bool control(int8_t T, int8_t currentT, time_t time)
{
	//u(t) = P(t) + I(t) + D(t)
	//P(t) = Cp * e(t)
	//I(t) = I(t � T) + Ci * e(t)
	//D(t) = Cd * (e(t) � e(t - T))

	const double err = (double)T - (double)currentT;

	if (isStarted == false)
	{
		prev.iT = 0.0;
		prev.err = err;
		prev.time = time;
		prev.u = false;
		isStarted = true;
		return false;
	}

	time_t deltaTime = time - prev.time;

	if (deltaTime < timeStep)
	{
		return prev.u;
	}

	double pT = kP * err;
	double iT = prev.iT + kI * (err + prev.err) * deltaTime / 2.0;

	if (iT > iTMax)
	{
		iT = iTMax;
	}
	else if (iT < iTMin)
	{
		iT = iTMin;
	}

	double dT = kD * (err - prev.err) / deltaTime;

	double u = pT + iT + dT;

	prev.iT = iT;
	prev.err = err;
	prev.time = time;
	prev.u = (bool)(u > U);

	return prev.u;
}
